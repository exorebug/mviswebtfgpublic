﻿using Lib_ISDispatcher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvisWeb.Services
{    
    public class QueryService
    {        
        public static bool getAdditionalInformation(string xmlQueryString, out string xmlResultString)
        {            
            IISDispatcher objectDispatcher = null;
            xmlResultString = "";

            try//Create COM Object
            {
                objectDispatcher = (IISDispatcher)new Lib_ISDispatcher.ISDispatcher();
            }
            catch (Exception p_oExc)
            {
                objectDispatcher = null;
                GC.Collect();
                return false;
            }

            try//Call COM Method
            {
                objectDispatcher.getAdditionalInformation(0, "IMAGE", xmlQueryString, out xmlResultString);
            }
            catch (Exception p_oExc)
            {
                objectDispatcher = null;
                GC.Collect();
                return false;
            }

            objectDispatcher = null;
            GC.Collect();
            return true;             
        }

        public static string getIconImage(string SOPInstanceUID)
        {
            
            IISDispatcher objectDispatcher = null;
            string objectPath = "";
            int result = 0; //No lo usamos, string vacío es ya un error de por si.

            try
            {
                objectDispatcher = (IISDispatcher)new Lib_ISDispatcher.ISDispatcher();
                objectDispatcher.getIconImage(SOPInstanceUID, out objectPath, out result);
            }
            //catch (Exception p_oExc) { }
            finally
            {
                objectDispatcher = null;
                GC.Collect();
            }
            return objectPath;
        }
    }
}
