﻿using System;
using MVClientCommon;
using MVClientSysConfManager;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using mvisWeb.Models;
using System.Web.Security;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace mvisWeb.Controllers
{
    //[Authorize]
    //[RequireHttps]
    public class AccountController : Controller
    {

        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            //HttpContext.GetOwinContext().Authentication.SignOut();
            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login");
        }
        
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<bool> Login(LoginViewModel model)
        public bool Login(LoginViewModel model)
        {
            //System.Diagnostics.Debug.WriteLine("DEBUG Loging In  user:" + model.userName + " password:" +  model.password );

            //login with MVIS
            MVClientSysConfManager.TLoginResults LoginResult;
            Imv_ClientManager ClientManager = null;
            ClientManager = (Imv_ClientManager)new MVClientCommon.mv_ClientManager();
            LoginResult = ((ImvCSM_User)ClientManager.User).logIn(model.userName.ToUpper(), model.password);

            //create identity for the logged user
            if (LoginResult == TLoginResults.lirSuccess)
            {
                var identity = new ClaimsIdentity(new [] {
                        new Claim(ClaimTypes.Name, model.userName),
                    },
                    DefaultAuthenticationTypes.ApplicationCookie,
                    ClaimTypes.Name, ClaimTypes.Role);

                // Addd roles, optional
                // identity.AddClaim(new Claim(ClaimTypes.Role, "guest"));

                // tell OWIN the identity provider, optional
                // identity.AddClaim(new Claim(IdentityProvider, "Simplest Auth"));

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = model.RememberMe,
                }, identity);

                return true;
            }

            return false;        
        }
    }
}