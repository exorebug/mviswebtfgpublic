﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvisWeb.Controllers
{
    //[Authorize]
    //[RequireHttps]
    public class ViewerController : Controller
    {        
        public ActionResult DicomViewer(string SOPInstanceUID)
        {
            string baseUrl = ConfigurationManager.AppSettings["wadoServiceURL"];
            //ViewBag.url = "http://localhost/wado/wadouri.aspx?requestType=WADO&objectUID=" + SOPInstanceUID + "&contentType=application/dicom";
            //ViewBag.url = "../wado/wadouri.aspx?requestType=WADO&objectUID=" + SOPInstanceUID + "&contentType=application/dicom";        
            //ViewBag.url = "http://localhost:53897/wadouri.aspx?requestType=WADO&objectUID=" + SOPInstanceUID + "&contentType=application/dicom";            
            ViewBag.url = baseUrl + "?requestType=WADO&objectUID=" + SOPInstanceUID + "&contentType=application/dicom";            
            return View();
        }

        public ActionResult VideoViewer(string SOPInstanceUID)
        {
            string baseUrl = ConfigurationManager.AppSettings["wadoServiceURL"];
            ViewBag.url = baseUrl + "?requestType=WADO&objectUID=" + SOPInstanceUID + "&contentType=application/mp4";                        
            return View();
        }
    }
}