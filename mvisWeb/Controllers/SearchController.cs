﻿using mvisWeb.Helpers;
using mvisWeb.Models;
using mvisWeb.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace mvisWeb.Controllers
{
    //[Authorize]
    //[RequireHttps]
    public class SearchController : Controller
    {
        public ActionResult SearchResultsTable()
        {
            return View();
        }

        public ActionResult SearchForm()
        {
            return View();
        }

        [HttpPost]
        public string SearchForm(SearchFormModel formData)
        {
            /*
            bool valid = ValidateFormData(formData);
            if (!valid)
            {
                return "Not valid";
            }
             */


            //Prepare xml documents for query/response
            XmlDocument xmlQuery = new XmlDocument();
            XmlDocument xmlResponse = new XmlDocument();

            //Load template and namespace
            xmlQuery.Load(Server.MapPath("~/Models/mviDicomFindTemplate.xml"));
            XmlNamespaceManager mviNamespace = new XmlNamespaceManager(xmlQuery.NameTable);
            mviNamespace.AddNamespace("mvi", "http://www.nte.es/MVI/1.0/XMLSchema");  

            //Select nodes for xml manipulation
            XmlNode root = xmlQuery.DocumentElement;
            XmlNode parentNode;
            XmlElement childNode;
            
            //TODO: Refactor the following code (set xml variables)
            //set name
            
            if (formData.name != null || formData.firstName != null || formData.secondName != null)
            {            
                parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='00100010']", mviNamespace);
                childNode = xmlQuery.CreateElement("elementValue", root.NamespaceURI);
                childNode.InnerText = "*" + formData.name + "*" + formData.firstName + "*" + formData.secondName + "*";
                parentNode.AppendChild(childNode);
            }
            
            //set nhc
            if (formData.nhc != null)
            {
                parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='00100020']", mviNamespace);
                childNode = xmlQuery.CreateElement("elementValue", root.NamespaceURI);
                childNode.InnerText = formData.nhc;
                parentNode.AppendChild(childNode);
            }            

            //set accessNum
            if (formData.accessNum != null)
            {
                parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='00080050']", mviNamespace);
                childNode = xmlQuery.CreateElement("elementValue", root.NamespaceURI);
                childNode.InnerText = formData.accessNum;
                parentNode.AppendChild(childNode);
            }
            

            //set modality
            if (formData.modalities != null)
            {
                parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='00080060']", mviNamespace);
                foreach (string modality in formData.modalities)
                {
                    childNode = xmlQuery.CreateElement("elementValue", root.NamespaceURI);
                    childNode.InnerText = modality;
                    parentNode.AppendChild(childNode);
                }                
            }
            

            //set date
            if (formData.startDate != null && formData.endDate != null)
            {
                parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='00080020']", mviNamespace);
                
                childNode = xmlQuery.CreateElement("rangeMin", root.NamespaceURI);
                childNode.InnerText = formData.startDate;
                parentNode.AppendChild(childNode);

                childNode = xmlQuery.CreateElement("rangeMax", root.NamespaceURI);
                childNode.InnerText = formData.endDate;
                parentNode.AppendChild(childNode);
            }            
            
            string stringXmlResponse;
            Response.ContentType = "application/json";

            //xmlQuery.Load(Server.MapPath("~/DebugFiles/multiModQuery.xml"));
            
            
            bool result = QueryService.getAdditionalInformation(xmlQuery.OuterXml, out stringXmlResponse);
            xmlResponse.LoadXml(stringXmlResponse);
            string resultString = JsonConvert.SerializeXmlNode(xmlResponse);

            bool hasResults = xmlResponse.SelectSingleNode("//mvi:queryResults", mviNamespace).HasChildNodes;

            if (hasResults)
            {
                resultString = JsonConvert.SerializeXmlNode(xmlResponse);
            }
            else
            {
                resultString = "false";                
            };


            //resultString = System.IO.File.ReadAllText(Server.MapPath("~/InternalWIPS_dontInclude/testfiles/QueryResponseMassiveUser.json"));
            
            /*
            xmlResponse.Load(Server.MapPath("~/DebugFiles/QueryResponseMultiUser.xml"));
            stringXmlResponse = xmlResponse.SelectSingleNode("//mvi:addInfoExchange", mviNamespace).InnerXml;
            xmlResponse.LoadXml(stringXmlResponse);
            resultJsonString = JsonConvert.SerializeXmlNode(xmlResponse);
            */
            return resultString;
        }

        public ActionResult getIcon(string SOPInstanceUID)
        {
            var path = QueryService.getIconImage(SOPInstanceUID);
            return base.File(path, "image/bmp"); // image/x-ms-bmp
        }

        /*
         * from this point onward everything is useless... for now?
         */
        private static void populateXmlNode(string formDataField, string tag, XmlDocument xmlQuery,
            XmlNamespaceManager mviNamespace, XmlNode root, XmlNode parentNode, XmlElement childNode)
        {
            parentNode = root.SelectSingleNode("//mvi:queryElement[@DICOMTag='"+tag+"']", mviNamespace);
            childNode = xmlQuery.CreateElement("elementValue", root.NamespaceURI);
            childNode.InnerText = formDataField;
            parentNode.AppendChild(childNode);
        }

        private static Dictionary<string, string> dicomTags = new Dictionary<string, string>()
        {
           {"name", "00100010"},        
           {"nhc", "00100020"},
           {"accessNum", "00080050"},   
           {"modality", "00080060"},   
           {"date", "00080020"},   
        };

        private static bool ValidateFormData(SearchFormModel formData){

            bool validated = true;
            bool isMatch;

            /*
            isMatch = Regex.IsMatch(formData.firstName, @"^[a-zA-Z ]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.secondName, @"^[a-zA-Z ]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.name, @"^[a-zA-Z ]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.nhc, @"^[0-9_-]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.accessNum, @"^[a-zA-Z0-9]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.modalities.ToString(), @"^[a-zA-Z]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.startDate, @"^[0-9]+$");
            if (!isMatch) return false;
            isMatch = Regex.IsMatch(formData.endDate, @"^[0-9]+$");
            if (!isMatch) return false;
            */

            return validated;
        }
    }
}