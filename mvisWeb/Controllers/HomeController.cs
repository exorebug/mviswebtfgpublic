﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using mvisWeb.Helpers;

namespace mvisWeb.Controllers
{
    //[Authorize]
    //[RequireHttps]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
             return View();
        }

        public ActionResult Toolbar()
        {
            return View();
        }
    }
}