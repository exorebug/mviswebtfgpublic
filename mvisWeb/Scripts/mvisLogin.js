﻿var mvisLogin = angular.module('mvisLogin', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'angularSpinner']);

mvisLogin.controller('LoginController', LoginController);
mvisLogin.factory('LoginFactory', LoginFactory);

var configFunction = function ($stateProvider, $httpProvider, $locationProvider) {

    $locationProvider.hashPrefix('!').html5Mode({
        enabled: true,
        requireBase: false
    });
    /*
    $stateProvider
        .state('default', {
            url: '/login'
        });*/
}

configFunction.$inject = ['$stateProvider', '$httpProvider', '$locationProvider'];

mvisLogin.config(configFunction);
