﻿var LoginController = function ($scope, $stateParams, $location, LoginFactory, $window, usSpinnerService) {
    $scope.loginForm = {
        userName: '',
        password: '',
        rememberMe: false,
        returnUrl: $stateParams.returnUrl,
        loginFailure: false,
        loginSuccess: false
    };

    $scope.login = function () {
        $scope.startSpin();
        $scope.loginForm.loginFailure = false;
        var result = LoginFactory($scope.loginForm.userName, $scope.loginForm.password, $scope.loginForm.rememberMe);
        result.then(function (result) {
            $scope.stopSpin();
            if (result.success) {
                $scope.loginForm.loginSuccess = true;
                $window.location.href = 'search';
            } else {
                $scope.loginForm.loginFailure = true;
            }
        });
    };

    $scope.spinneractive = false;

    $scope.startSpin = function () {
        if (!$scope.spinneractive) {
            usSpinnerService.spin('spinner-1');
            $scope.spinneractive = true;
        }
    };

    $scope.stopSpin = function () {
        if ($scope.spinneractive) {
            usSpinnerService.stop('spinner-1');
            $scope.spinneractive = false;
        }
    };
}

LoginController.$inject = ['$scope', '$stateParams', '$location', 'LoginFactory', '$window', 'usSpinnerService'];