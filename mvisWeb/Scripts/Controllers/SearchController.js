﻿var SearchController = function ($scope, $http, $location, $state, $filter, $uibModal, uibDatepickerPopupConfig, usSpinnerService) {

    $scope.patients = [];

    $scope.isOpen = {
        calStart: false,
        calEnd: false
    };

    $scope.calStartOpen = function ($event) {
        $scope.isOpen.calStart = true;
    };
    $scope.calEndOpen = function ($event) {
        $scope.isOpen.calEnd = true;
    };

    uibDatepickerPopupConfig.datepickerPopup = 'dd/MM/yyyy';
    uibDatepickerPopupConfig.currentText = 'Hoy';
    uibDatepickerPopupConfig.clearText = 'Borrar';
    uibDatepickerPopupConfig.closeText = 'Cerrar';

    $scope.modalities = [
        'IVUS',
        'XA',
        'CA'
    ];

    $scope.searchFormData = {
        firstName : '',
        secondName :'',
        name: '',
        nhc : '',
        accessNum: '',
        modalities: [],
        dateSelection: false,
        startDate: '',
        endDate: ''
    };

    $scope.$watch('searchFormData.dateSelection', function () {
        if ($scope.searchFormData.dateSelection == true) {
            $scope.searchFormData.endDate = new Date();
        } else {
            $scope.resetDates();
        };
    });

    $scope.resetDates = function () {
        $scope.searchFormData.startDate = null;
        $scope.searchFormData.endDate = null;
    };

    $scope.toggleSelection = function toggleSelection(modality) {
        var idx = $scope.searchFormData.modalities.indexOf(modality);
        // is currently selected
        if (idx > -1) {
            $scope.searchFormData.modalities.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.searchFormData.modalities.push(modality);
        }
    };

    $scope.searchFailed = false;

    $scope.search = function () {
        $scope.searchFailed = false;
        $scope.startSpin();        
        if ($scope.searchFormData.dateSelection == false) {
            $scope.resetDates();
        } else {
            $scope.searchFormData.startDate = $filter('date')($scope.searchFormData.startDate, 'yyyyMMdd');
            $scope.searchFormData.endDate = $filter('date')($scope.searchFormData.endDate, 'yyyyMMdd');
        }        
        $http({
            method: 'POST',
            url: 'searchForm',
            data: $scope.searchFormData            
        }).then(function (response) {
            $scope.stopSpin();
            if (response.data == false) {
                $scope.searchFailed = true;
            } else {
                $scope.patients = response.data;
                $state.transitionTo('default.search', { arg: 'arg' });
            }            
        });
    };

    $scope.testArray = function (obj) {
        if (angular.isArray(obj)) {
            return obj;
        } else {
            return [obj];
        }
    };

    $scope.spinneractive = false;

    $scope.startSpin = function () {
        if (!$scope.spinneractive) {
            usSpinnerService.spin('spinner-1');
            $scope.spinneractive = true;
        }
    };

    $scope.stopSpin = function () {
        if ($scope.spinneractive) {
            usSpinnerService.stop('spinner-1');
            $scope.spinneractive = false;
        }
    };

    $scope.openHelp = function () {
        $uibModal.open({
            templateUrl: 'searchHelp.html',
            controller: ModalInstanceCtrl,
            scope: $scope
        });
    };

    var ModalInstanceCtrl = function ($scope, $uibModalInstance) {
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    };
    ModalInstanceCtrl.$inject = ['$scope', '$uibModalInstance'];
    
    //dummy test method for alert triggering on angular directives
    $scope.testAlert = function () {
        alert("test!");
    };

    $scope.testStorage = function (data) {
        sessionStorage["dadatam8"] = JSON.stringify(data);
        var mydata = sessionStorage["dadatam8"];
        var dummy = "";
    };

}

SearchController.$inject = ['$scope', '$http', '$location', '$state', '$filter', '$uibModal', 'uibDatepickerPopupConfig', 'usSpinnerService'];