﻿var mvisWeb = angular.module('mvisWeb', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'angularSpinner']);

mvisWeb.controller('SearchController', SearchController);

var configFunction = function ($stateProvider, $httpProvider, $locationProvider) {
        
    $locationProvider.hashPrefix('!').html5Mode({
        enabled: true,
        requireBase: false
    });
    
    $stateProvider
        .state('default', {
            url: '/search',
            views: {
                "mainContainer": {
                    controller: SearchController
                },
                "contentWindow@default": {
                    templateUrl: 'searchForm'
                }
            }
        })
        .state('default.search', {
            url: '/results/',
            views: {
                "contentWindow@default": {
                    templateUrl: 'searchResultsTable'
                }
            }
        });
}

configFunction.$inject = ['$stateProvider', '$httpProvider', '$locationProvider'];

mvisWeb.config(configFunction);