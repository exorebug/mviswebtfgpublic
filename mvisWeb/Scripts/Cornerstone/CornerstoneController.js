﻿/// <reference path="_references.js" />

var CornerstoneController = function ($scope, $http) {

    var element = document.getElementById('dicomImage');

    var imageIds = [];

    $scope.stack = {
        currentImageIdIndex: 0,
        imageIds: imageIds
    };

    $scope.patient = {
        name: "",
        birthDate: "",
        studyDate: ""
    };

    $scope.frames = 0;
    $scope.fps = 20;
    $scope.playLoop = true;
    $scope.currentImgIdxSlider = 0;
    $scope.isPlay = false;
    $scope.isStop = true;

    //Controller init function
    $scope.init = function (url) {
        $scope.wadoUrl = "wadouri:" + url;
        $scope.viewer();
    };   
    
    //viewer image load function
    $scope.viewer = function () {
        cornerstone.loadAndCacheImage($scope.wadoUrl).then(function (image) {
            // Enable the dicomImage element and the mouse inputs
            cornerstone.enable(element);

            //get image information
            $scope.patient.name = image.data.string('x00100010');
            $scope.patient.birthDate = image.data.string('x00100030');
            $scope.patient.studyDate = image.data.string('x00080020');
            
            $scope.frames = parseInt(image.data.string('x00280008'));
            var frameTime = image.data.string('x00181063');
            var recomendedFrameRate = image.data.string('x00082144');
            if (typeof frameTime !== 'undefined') {
                $scope.fps = Math.round(1000 / parseInt(frameTime));                
            } else if (typeof recomendedFrameRate !== 'undefined') {
                $scope.fps = parseInt(recomendedFrameRate);
            }                      

            for (var i = 0; i < $scope.frames; i++) {
                $scope.stack.imageIds.push($scope.wadoUrl + '?frame=' + i);
            }

            // Initialize range input
            //var range, max, slice, currentValueSpan;
            $scope.range = document.getElementById('slice-range');            
            // Set minimum and maximum value
            $scope.range.min = 1;
            $scope.range.step = 1;
            $scope.range.max = imageIds.length;            
            // Set current value
            //$scope.range.value = $scope.stack.currentImageIdIndex;                        

            $scope.$apply();

            // Display the image
            cornerstone.displayImage(element, image);
            cornerstoneTools.mouseInput.enable(element);
            cornerstoneTools.mouseWheelInput.enable(element);

            // Set the stack as tool state
            cornerstoneTools.addStackStateManager(element, ['stack', 'playClip']);
            cornerstoneTools.addToolState(element, 'stack', $scope.stack);            

            // Set default tools we want to use with this element            
            cornerstoneTools.stackScroll.activate(element, 1);
            cornerstoneTools.stackScrollWheel.activate(element);
            cornerstoneTools.scrollIndicator.enable(element);

            //var image = cornerstone.getImage(element);
            $scope.viewport = cornerstone.getViewport(element);
            $scope.viewport.modalityLUT = undefined;
            cornerstone.setViewport(element, $scope.viewport);

            //Watch image change event
            $(element).on("CornerstoneNewImage", $scope.onNewImage);
            $(window).resize();
        });
    };
    
    $scope.onNewImage = function (event, data) {
        // Update the slider value        
        $scope.currentImgIdxSlider = $scope.stack.currentImageIdIndex + 1;
        if (!$scope.$$phase) {
            $scope.$apply();
        }        
    };        

    $scope.selectImage = function (newImageIdIndex) {
        // Switch images, if necessary
        if ($scope.stack.imageIds[newImageIdIndex] !== undefined) {
            cornerstone.loadAndCacheImage($scope.stack.imageIds[newImageIdIndex]).then(function (image) {
                $scope.viewport = cornerstone.getViewport(element);
                $scope.stack.currentImageIdIndex = parseInt(newImageIdIndex,10);
                cornerstone.displayImage(element, image, $scope.viewport);
            });
        }
    };

    $scope.previous = function () {
        var result = parseInt($scope.stack.currentImageIdIndex,10)-1;
        $scope.selectImage(result.toString());
    };

    $scope.next = function () {
        var result = parseInt($scope.stack.currentImageIdIndex,10)+1;
        $scope.selectImage(result.toString());
    };

    $scope.playPauseClip = function () {
        if ($scope.isPlay) {
            $scope.isPlay = false;
            cornerstoneTools.stopClip(element);
        } else {
            $scope.isPlay = true;
            cornerstoneTools.playClip(element, $scope.fps);
        }
    };

    $scope.stopClip = function () {
        cornerstoneTools.stopClip(element);
        $scope.selectImage("0");
        $scope.isPlay = false;
    };

    $scope.enableLoop = function () {
        var playClipToolData = cornerstoneTools.getToolState(element, 'playClip');
        playClipToolData.data[0].loop = $scope.playLoop;
    };

    $scope.test = function () {
        alert("test");
    };

    $scope.changeTools = function (tool, event) {
        resetAllTools();
        switch (tool) {
            case 'ww':
                cornerstoneTools.wwwc.activate(element, 1);
                cornerstoneTools.wwwcTouchDrag.activate(element);
                break;
            case 'invert':
                $scope.viewport = cornerstone.getViewport(element);
                if ($scope.viewport.invert === true) {
                    $scope.viewport.invert = false;
                } else {
                    $scope.viewport.invert = true;
                }
                cornerstone.setViewport(element, $scope.viewport);
                break;
            case 'zoom':
                cornerstoneTools.zoom.activate(element, 1); // 5 is right mouse button and left mouse button
                cornerstoneTools.zoomTouchDrag.activate(element);
                break;
            case 'pan':
                cornerstoneTools.pan.activate(element, 1); // 3 is middle mouse button and left mouse button
                cornerstoneTools.panTouchDrag.activate(element);
                break;
            case 'scroll':
                cornerstoneTools.stackScroll.activate(element, 1);
                cornerstoneTools.stackScrollTouchDrag.activate(element);
                break;
            default:
                //
        }
    };

    //Cornerstone tools buttons: {1:left, 2:middle, 3:midle+left, 4:right, 5:right+left}
    function resetAllTools() {
        cornerstoneTools.wwwc.disable(element);     
        cornerstoneTools.stackScroll.deactivate(element);
        cornerstoneTools.wwwcTouchDrag.deactivate(element);
        cornerstoneTools.zoomTouchDrag.deactivate(element);
        cornerstoneTools.panTouchDrag.deactivate(element);
        cornerstoneTools.stackScrollTouchDrag.deactivate(element);
        cornerstoneTools.pan.activate(element, 2); // 2 is middle mouse button
        cornerstoneTools.zoom.activate(element, 4); // 4 is right mouse button   
        /*
        cornerstoneTools.probe.deactivate(element);
        cornerstoneTools.length.deactivate(element);
        cornerstoneTools.angle.deactivate(element);
        cornerstoneTools.ellipticalRoi.deactivate(element);
        cornerstoneTools.rectangleRoi.deactivate(element);
         */

    };

    $(window).resize(function () {
        /*
        console.log("width " + element.clientWidth);
        console.log("height " + element.clientHeight);
        
        var viewportHeight = window.innerHeight ? window.innerHeight : $(window).height();
        console.log("vp h: " + viewportHeight);
        
        if ($('#imageBox').height() > viewportHeight) {
            $('.viewport').height($('#imageBox').height() - viewportHeight);
        }
        if ($('#imageBox').height() < viewportHeight) {
            $('.viewport').height(viewportHeight - $('#imageBox').height());
        }
        */
        var ibox = document.getElementById('imageBox');
        $('.viewport').height(ibox.clientWidth);
        $('.viewport').width(ibox.clientHeight);
        cornerstone.resize(element, true);
    });
}

CornerstoneController.$inject = ['$scope', '$http'];