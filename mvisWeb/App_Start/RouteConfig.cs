﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace mvisWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Index",
                url: "search",
                defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
                name: "Toolbar",
                url: "toolbar",
                defaults: new { controller = "Home", action = "Toolbar" });

            routes.MapRoute(
                name: "SearchForm",
                url: "searchForm",
                defaults: new { controller = "Search", action = "SearchForm" });

            routes.MapRoute(
                name: "SearchResultsTable",
                url: "searchResultsTable",
                defaults: new { controller = "Search", action = "SearchResultsTable" });

            routes.MapRoute(
                name: "GetIcon",
                url: "getIcon",
                defaults: new { controller = "Search", action = "getIcon" });

            routes.MapRoute(
                name: "DicomViewer",
                url: "dicomViewer",
                defaults: new { controller = "Viewer", action = "DicomViewer", SOPInstanceUID = UrlParameter.Optional });

            routes.MapRoute(
                name: "VideoViewer",
                url: "videoViewer",
                defaults: new { controller = "Viewer", action = "VideoViewer", SOPInstanceUID = UrlParameter.Optional });


            routes.MapRoute(
               name: "Login",
               url: "login",
               defaults: new { controller = "Account", action = "Login" });

            routes.MapRoute(
               name: "Logout",
               url: "logout",
               defaults: new { controller = "Account", action = "Logout" });
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );            
        }
    }
}
