﻿using System.Web;
using System.Web.Optimization;

namespace mvisWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //main app sources
            bundles.Add(new ScriptBundle("~/bundles/mvisWeb")
                .IncludeDirectory("~/Scripts/Controllers", "*.js")
                .IncludeDirectory("~/Scripts/Factories", "*.js")
                .Include("~/Scripts/mvisWeb.js"));

            //login app sources
            bundles.Add(new ScriptBundle("~/bundles/mvisLogin")
                .Include("~/Scripts/Controllers/LoginController.js")
                .Include("~/Scripts/Factories/LoginFactory.js")
                .Include("~/Scripts/mvisLogin.js"));

            //angular sources for developement
            bundles.Add(new ScriptBundle("~/bundles/angularDebug")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/i18n/angular-locale_es-es.js")  
                .Include("~/Scripts/angular-ui-router.js")
                .Include("~/Scripts/angular-animate.js")
                .Include("~/Scripts/ui-bootstrap-tpls.js")
                .Include("~/Scripts/spin.js")
                .Include("~/Scripts/angular-spinner.js"));

            //angular sources minified
            bundles.Add(new ScriptBundle("~/bundles/angularMin")
                .Include("~/Scripts/angular.min.js")
                .Include("~/Scripts/i18n/angular-locale_es-es.js") 
                .Include("~/Scripts/angular-ui-router.min.js")
                .Include("~/Scripts/angular-animate.min.js")
                .Include("~/Scripts/ui-bootstrap-tpls.min.js")
                .Include("~/Scripts/spin.min.js")
                .Include("~/Scripts/angular-spinner.min.js"));

            //cornerstone core, tools and dependencies
            bundles.Add(new ScriptBundle("~/bundles/cornerstoneCore")
                .Include("~/Scripts/jquery.js")
                .Include("~/Scripts/Cornerstone/cornerstone.js")
                .Include("~/Scripts/Cornerstone/cornerstoneMath.js")
                .Include("~/Scripts/Cornerstone/cornerstoneTools.js"));

            //cornerstone WADO loader and dependencies
            bundles.Add(new ScriptBundle("~/bundles/cornerstoneWADO")
                .Include("~/Scripts/Cornerstone/dicomParser.js")
                .Include("~/Scripts/Cornerstone/jpx.js")
                .Include("~/Scripts/Cornerstone/cornerstoneWADOImageLoader.js"));

            //cornerstone application and dependencies
            bundles.Add(new ScriptBundle("~/bundles/cornerstoneApplication")
                .Include("~/Scripts/angular.min.js")
                .Include("~/Scripts/Cornerstone/cornerstoneController.js")
                .Include("~/Scripts/Cornerstone/cornerstoneApp.js"));

            //main css styles
            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/font-awesome.min.css")    
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/iefix")
                .Include("~/Content/iexplorer-https-fonts-fix.css"));

            //styles for cornerstone
            bundles.Add(new StyleBundle("~/Content/cornerstoneCSS")
                .Include("~/Content/font-awesome.min.css")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/cornerstone.css")
                .Include("~/Content/site.css"));

            //styles for video-js
            bundles.Add(new StyleBundle("~/Content/videoJS")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Content/video-js.min.css")
                .Include("~/Content/video-js-custom.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
