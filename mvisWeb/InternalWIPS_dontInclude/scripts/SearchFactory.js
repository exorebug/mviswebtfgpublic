﻿var SearchFactory = function ($http, $q) {
    return function (firstName, secondName, name, nhc, accessNum) {
        var deferredObject = $q.defer();
        $http({
            method: 'POST',
            url: 'Manage/SearchForm',
            data: {
                'firstName': firstName,
                'secondName': secondName,
                'name': name,
                'nhc': nhc,
                'accessNum': accessNum
            }
        }).success(function (result) {
            deferredObject.resolve({ success: true });           
        }).error(function () {
            deferredObject.resolve({ success: false });
        });
        return deferredObject.promise;
    }
}

SearchFactory.$inject = ['$http', '$q'];

/*
$http.post(
            'Manage/SearchForm', {
                UserName: userName,
                Password: password
            }
        )*/