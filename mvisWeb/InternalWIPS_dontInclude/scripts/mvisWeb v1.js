﻿var mvisWeb = angular.module('mvisWeb', ['ui.router', 'ui.bootstrap', 'ngAnimate']);

mvisWeb.controller('SearchController', SearchController);
mvisWeb.factory('SearchFactory', SearchFactory);

var configFunction = function ($stateProvider, $httpProvider, $locationProvider) {

    $locationProvider.hashPrefix('!').html5Mode({
        enabled: true,
        requireBase: false
    });

    $stateProvider
        .state('main', {
            url: '/',
            views: {
                "mainContainer": {
                    controller: SearchController
                },
                "toolsContainer@main": {
                    templateUrl: '/manage/searchForm'
                },
                "contentWindow@main": {
                    templateUrl: '/home/placeholder'
                }
            }
        })
        .state('main.patientTable', {
            url: 'searchResults',
            views: {
                "contentWindow@main": {
                    templateUrl: '/manage/mainTable'
                }
            }
        });
}

configFunction.$inject = ['$stateProvider', '$httpProvider', '$locationProvider'];

mvisWeb.config(configFunction);