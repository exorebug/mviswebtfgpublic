﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Xml;

namespace mvisWeb.Helpers
{
    public class xmlJsonConverter
    {

        public string mviXmlToJson(XmlDocument doc)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(@"Models/QueryResponse.xml");
            //doc.Load(@"C:/testfiles/QueryResponse.xml");

            XmlNamespaceManager namespaces = new XmlNamespaceManager(doc.NameTable);
            namespaces.AddNamespace("mvi", "http://www.nte.es/MVI/1.0/XMLSchema");            
            string jsonText = JsonConvert.SerializeXmlNode(doc);

            return jsonText;
        }

        public string jsonToXml()
        {

            

            return "";
        }
    }
}