﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvisWeb.Startup))]
namespace mvisWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
