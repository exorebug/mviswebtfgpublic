﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace mvisWeb.Models
{

    public class SearchResultsTableModel
    {
        //todo: may not be necessary
    }

    public class SearchFormModel
    {              
        [DataType(DataType.Text)]
        [Display(Name = "firstName")]
        public string firstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "secondName")]
        public string secondName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "name")]
        public string name { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "nhc")]
        public string nhc { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "accessNum")]
        public string accessNum { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "modalities")]
        public List<string> modalities { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "startDate")]
        public string startDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "endDate")]
        public string endDate { get; set; }
    }
    
}