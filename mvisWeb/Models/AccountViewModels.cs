﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace mvisWeb.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "userName")]
        public string userName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string password { get; set; }

        public bool RememberMe { get; set; }

    }
}