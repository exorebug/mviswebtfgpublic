﻿using System;
using mvisWeb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mvisWeb.Controllers;
using System.Web.Mvc;
using System.Web;

namespace mvisWebUnitTest
{
    [TestClass]
    public class AccountControllerTests
    {

        [TestMethod]
        public void LoginView()
        {
            AccountController accCtrl = new AccountController();
            ActionResult result = accCtrl.Login();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Login()
        {

        }

        [TestMethod]
        public void Logout()
        {            
            // mock login object??
            AccountController accCtrl = new AccountController();
            ActionResult result = accCtrl.Logout();
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            RedirectToRouteResult routeResult = result as RedirectToRouteResult;
            Assert.AreEqual(routeResult.RouteValues["action"], "login");
        }
    }
}
