﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mvisWeb.Controllers;
using System.Web.Mvc;

namespace mvisWebUnitTest
{
    [TestClass]
    public class SearchControllerTests
    {
        [TestMethod]
        public void SearchResultsTableView()
        {
            SearchController searchCtrl = new SearchController();
            ActionResult result = searchCtrl.SearchResultsTable();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void SearchFormView()
        {        
            SearchController searchCtrl = new SearchController();
            ActionResult result = searchCtrl.SearchForm();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void SearchForm()
        {
            //to do
            /*
             * mocks needed:
             * model data
             * query service?????
             * 
             */
        }
    }
}
