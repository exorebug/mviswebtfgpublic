﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mvisWeb.Controllers;
using System.Web.Mvc;

namespace mvisWebUnitTest
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void IndexView()
        {
            HomeController homeCtrl = new HomeController();
            ActionResult result = homeCtrl.Index();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
        [TestMethod]
        public void ToolbarView()
        {
            HomeController homeCtrl = new HomeController();
            ActionResult result = homeCtrl.Toolbar();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
